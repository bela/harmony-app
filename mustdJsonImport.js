const emojiList = require(process.env.MUSTD_PATH);
const fs = require("fs");

const output = emojiList.cats;

for (const category in output) {
  output[category] = output[category].map(emoji => {
    const color = emojiList.roots[emoji];
    const shape = color && emojiList.roots[emoji].includes("clw_default");

    if (color && shape) emoji += "#";
    else if (shape) emoji += "@";
    else if (color) emoji += "!";

    return emoji;
  });
}

fs.writeFileSync("./emojiCategories.json", JSON.stringify(output), "utf8");
