function convertBlobToArrayBuffer(blob) {
  return new Promise(resolve => {
    const fileReader = new FileReader();
    fileReader.addEventListener("load", () => {
      resolve(fileReader.result);
    });
    fileReader.readAsArrayBuffer(blob);
  });
}

export default convertBlobToArrayBuffer;
