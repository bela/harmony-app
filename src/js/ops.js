export default {
  hello: 0,
  heartbeat: 1,
  subscribe: 2,
  unsubscribe: 3,
  dispatch: 4,
  presence: 5
};
