import emojis from "./emojis.js";

export default {
  urlFor(emoji) {
    if (emojis.includes(emoji)) {
      return "https://mutant.lavatech.top/shortcode/svg/" + emoji + ".svg";
    } else {
      return null;
    }
    // return `https://mutant.tech/-rsc/emoji/mutstd/${emoji}.svg`;
  },
  diversify(emoji, color, shape) {
    let name = emoji.n;
    if (emoji.s) name += "_" + shape;
    if (emoji.c) name += "_" + color;
    return name;
  }
};
