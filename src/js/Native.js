let nonceCounter = 0;
const waiting = new Map();

window.addEventListener("message", ev => {
  console.log("MESSAGE!", ev);
  if (!ev.data.reply) {
    return console.warn("Got our own message back");
  }
  if (ev.data.nonce !== undefined) {
    const cb = waiting.get(ev.data.nonce);
    if (cb) {
      console.log(ev.data.body, ev.data);
      cb(JSON.parse(ev.data.body));
    }
  }
});

export async function preloadPost(action, data) {
  const nonce = nonceCounter++;
  const payload = {action, nonce};
  if (data) {
    payload.data = data;
  }
  window.postMessage(payload);
  return await new Promise((resolve, reject) => {
    waiting.set(nonce, response => {
      cleanup();
      resolve(response);
    });
    function cleanup() {
      waiting.delete(nonce);
      clearTimeout(timeout);
    }
    const timeout = setTimeout(() => {
      cleanup();
      console.error("No response from payload", payload);
      reject(new Error("Timed out waiting for response!"));
    }, 60000);
  });
}

export async function enumerateDisplays() {
  return await preloadPost("enumerateDisplays");
}

export const isNative = navigator.userAgent.includes("Electron/");
