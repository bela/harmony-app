import * as keyval from "idb-keyval";

const self = typeof window === "undefined" ? self : window;

export default {
  async getToken() {
    if (self.localStorage && self.localStorage.token) {
      return self.localStorage.token;
    } else {
      const token = await keyval.get("token");
      if (token && self.localStorage) {
        self.localStorage.token = token;
      }
      return token;
    }
  },
  async getUserId(token) {
    if (!token) {
      token = await this.getToken();
    }
    return this.getUserIdSync(token);
  },
  getUserIdSync(token) {
    if (!token) {
      token = this.getTokenSync();
    }
    if (token) {
      return token.substring(0, token.indexOf("."));
    }
  },
  getTokenSync() {
    return self.localStorage && self.localStorage.token;
  },
  async setToken(token) {
    if (self.localStorage) {
      self.localStorage.token = token;
    }
    await keyval.set("token", token);
  }
};
