import * as keyval from "idb-keyval";
import constants from "./constants";
import RestClient from "./RestClient";

// Not sure this is necessary but it's better to be safe than sorry I suppose
if (typeof window !== "undefined") {
  window.self = window;
}

async function getKey(keyName, keyType) {
  if (keyType !== "public" && keyType !== "private") {
    throw new Error("keyType must be one of public,private");
  }
  let returnKey = await keyval.get(`cryptipush-${keyName}-${keyType}`);
  if (!returnKey) {
    const key = await self.crypto.subtle.generateKey(
      {name: "ECDSA", namedCurve: "P-256"},
      false,
      ["sign"]
    );
    await keyval.set(`cryptipush-${keyName}-private`, key.privateKey);
    await keyval.set(`cryptipush-${keyName}-public`, key.publicKey);
    returnKey = key[keyType + "Key"];
  }
  console.log("[cryptipush] got key!", returnKey);
  return returnKey;
}

export async function signObject(payload) {
  const packet = JSON.stringify(payload);
  const buffer = str2ab(packet);
  const privateKey = await getKey("push", "private");
  const signature = await self.crypto.subtle.sign(
    {name: "ECDSA", hash: {name: "SHA-256"}},
    privateKey,
    buffer
  );
  return signature;
}

export async function getWorkerRegistration() {
  if (typeof window === "undefined") {
    return self.registration;
  } else {
    return await navigator.serviceWorker.getRegistration();
  }
}

export async function removeSubscription(subscription) {
  if (subscription === undefined) {
    const registration = await getWorkerRegistration();
    subscription = await registration.pushManager.getSubscription();
  }
  if (subscription) {
    try {
      await subscription.unsubscribe();
    } catch (err) {
      return console.error("Error unsubscribing...", err);
    }
  }

  await publishSubscription(null);
}

export async function subscribe() {
  console.log("[cryptipush] Subscribing");
  const registration = await getWorkerRegistration();
  console.log("[cryptipush] Got registration, subscribing", registration);
  return await registration.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: constants.vapid.publicKey
  });
}

export async function getPublicKeyRaw() {
  const publicKey = await getKey("push", "public");
  const publicKeyRaw = await self.crypto.subtle.exportKey("raw", publicKey);
  return publicKeyRaw;
}

// We need something nicer to put in our URIs as IDs
export async function getDeviceId() {
  const publicKeyRaw = await getPublicKeyRaw();
  console.log("PublicKeyRaw!", publicKeyRaw);
  const keyDigest = await crypto.subtle.digest("SHA-512", publicKeyRaw);
  return ab2b64(keyDigest);
}

function ab2b64(buffer) {
  let binary = "";
  const bytes = new Uint8Array(buffer);
  const length = bytes.byteLength;
  for (let i = 0; i < length; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  return self.btoa(binary);
}

export async function publishSubscription(subscription) {
  if (subscription === undefined) {
    const registration = await getWorkerRegistration();
    subscription = await registration.pushManager.getSubscription();
  }
  const subscriptionData = subscription === null ? null : subscription.toJSON();
  const signature = await signObject(subscriptionData);
  const publicKeyRaw = await getPublicKeyRaw();
  const subscriptionPayload = {
    signature: ab2b64(signature),
    data: subscriptionData,
    key: ab2b64(publicKeyRaw)
  };
  await RestClient.patch("/push/subscription", subscriptionPayload, true);
  console.log("[cryptipush] Updated push subscription");
}

// Thanks Google: https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String
function str2ab(str) {
  const buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
  const bufView = new Uint16Array(buf);
  for (let i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}
