import device from "./device";
import slide from "svelte-transitions-slide";

export default function mobileSlide(node, data) {
  if (device.isMobileView()) {
    return slide(node, data);
  } else {
    return {
      duration: 0,
      css: () => {
        console.log("css");
      }
    };
  }
}
