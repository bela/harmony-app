import {EventEmitter} from "events";
import UserStore from "./UserStore";
import RealTimeClient from "../js/RealTimeClient";
import MessageStore from "./MessageStore";

class MemberStore extends EventEmitter {
  constructor() {
    super();
    this.storage = new Map();
  }

  slurp(member) {
    console.log("SLURPING A MEMBER!", member);
    const existing = this.storage.get(member.channelId);
    if (existing) existing.add(member.id);
    else this.storage.set(member.channelId, new Set([member.id]));
  }

  resolve(username, channelId) {
    console.log("RESOLVING", username, "FOR", channelId, "WITH", this.storage);
    const channelMembers = this.storage.get(channelId);
    if (!channelMembers) return null;
    for (const userId of channelMembers.values()) {
      const user = UserStore.getUser(userId);
      if (user.username === username) return user;
    }
  }

  search(query, state) {
    if (state.size === 0) {
      state.clear();
      return state;
    }
    if (state instanceof String) {
      state = new Map();
      const users = this.storage.get(state).values();
      for (const userId of users) {
        state.add(UserStore.getUser(userId) || UserStore.fetchUser(userId));
      }
    }

    for (const [id, user] of state.entries()) {
      // Ignore it for now I guess...
      if (user instanceof Promise) continue;
      if (!user.username.startsWith(query)) state.delete(id);
    }
  }
}

const store = new MemberStore();

MessageStore.on("new", function(message) {
  console.log("mzw", message);
  if (message.authorId) {
    store.slurp({
      id: message.authorId,
      channelId: message.channelId
    });
  }
});

window.memstore = store;

export default store;
