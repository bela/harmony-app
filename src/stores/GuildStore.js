import RealTimeStore from "./RealTimeStore";
import RestClient from "../js/RestClient";

class GuildStore extends RealTimeStore {
  constructor() {
    super();
    super._setup();
  }

  storeName = "GUILD";
  subscriptionKey = "g";

  getGuild(guildId) {
    return this.getItem(guildId);
  }

  getGuilds() {
    return this._storage;
  }

  async _fetchData(guildId) {
    console.log("Fetching...", guildId);
    return await RestClient.get("/guilds");
  }
}

export default new GuildStore();
