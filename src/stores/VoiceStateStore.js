import {EventEmitter} from "events";
import UserStore from "./UserStore";
import RealTimeClient from "../js/RealTimeClient";

class VoiceStateStore extends EventEmitter {
  constructor() {
    super();
    this.storage = new Map();
    this.metaStore = new Map();
  }

  slurp(voiceStates) {
    console.log("fuck were slurping VOICESTATES");
    let chosenVoiceState = null;
    if (voiceStates.id) {
      this._slurpOne(voiceStates);
      chosenVoiceState = voiceStates;
    } else {
      for (const voiceState of voiceStates.values
        ? voiceStates.values()
        : voiceStates) {
        this._slurpOne(voiceState);
        chosenVoiceState = voiceState;
      }
    }
    if (chosenVoiceState !== null) this.emit("changed", chosenVoiceState);
  }

  unslurp(voiceStates) {
    console.log("fuck were unslurping VOICESTATES");
    let chosenVoiceState = null;
    if (voiceStates.id) {
      this._unslurpOne(voiceStates);
      chosenVoiceState = voiceStates;
    } else {
      for (const voiceState of voiceStates.values
        ? voiceStates.values()
        : voiceStates) {
        this._unslurpOne(voiceState);
        chosenVoiceState = voiceState;
      }
    }
    if (chosenVoiceState !== null) this.emit("changed", chosenVoiceState);
  }

  _unslurpOne(voiceState) {
    const existed = this.storage.delete(voiceState.id);
    // No point continuing if it doesn't exist in one storage map
    if (existed) {
      const metaGot = this.metaStore.get(voiceState.channelId);
      if (metaGot) metaGot.delete(voiceState.id);
      this.emit(`updated_${voiceState.id}`, null);
    }
  }

  _slurpOne(voiceState) {
    if (voiceState.author) UserStore._slurpOne(voiceState.author);
    console.log("one slurperoo");
    const oldVoiceState = this.storage.get(voiceState.id);
    const finalVoiceState = {
      ...(oldVoiceState || {}),
      ...voiceState
    };
    this.storage.set(voiceState.id, finalVoiceState);
    if (voiceState.channelId) {
      const oldMeta = this.metaStore.get(voiceState.channelId);
      if (oldMeta) oldMeta.add(voiceState.id);
      else this.metaStore.set(voiceState.channelId, new Set([voiceState.id]));
    }
    if (oldVoiceState) {
      this.emit(`updated_${voiceState.id}`, finalVoiceState);
    } else {
      this.emit("new", finalVoiceState);
    }
  }

  getVoiceStates(channelId, asArray) {
    const result = {};
    if (channelId) {
      const voiceStateIds = this.metaStore.get(channelId);

      if (asArray) {
        if (!voiceStateIds || !voiceStateIds.size) return [];
        console.log(Array.from(voiceStateIds.values()).sort());
        return Array.from(voiceStateIds.values())
          .sort()
          .reverse()
          .map(voiceStateId => this.storage.get(voiceStateId));
      }

      if (!voiceStateIds) return {};
      for (const voiceStateId of voiceStateIds.values()) {
        result[voiceStateId] = this.storage.get(voiceStateId);
      }
      return result;
    } else {
      if (asArray) return Array.from(this.storage.values());
      for (const voiceState of this.storage.values()) {
        result[voiceState.id] = voiceState;
      }
      return result;
    }
  }

  getVoiceState(voiceStateId) {
    return this.storage.get(voiceStateId);
  }
}

RealTimeClient.on("VOICESTATE_UPDATE", function(msg) {
  console.log("UPDATE! yum yum");
  store.slurp(msg);
});

RealTimeClient.on("VOICESTATE_CREATE", function(msg) {
  console.log("slurp yum yum");
  store.slurp(msg);
});

RealTimeClient.on("VOICESTATE_DELETE", function(msg) {
  console.log("DELETE yum yum");
  store.unslurp(msg);
});

const store = new VoiceStateStore();
export default store;
