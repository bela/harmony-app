import {EventEmitter} from "events";
import RealTimeClient from "../js/RealTimeClient";
import constants from "../js/constants";

class GenericStore extends EventEmitter {
  constructor() {
    super();
  }

  _setup() {
    // The inheritor is supposed to call _setup();
    this._createStorages();
    this._attachListeners();
  }

  _attachListeners() {
    RealTimeClient.on(`${this.storeName}_CREATE`, this.slurp.bind(this));
    RealTimeClient.on(`${this.storeName}_UPDATE`, this.slurp.bind(this));
    RealTimeClient.on(`${this.storeName}_DELETE`, this.unslurp.bind(this));
  }

  _createStorages() {
    this._storage = new Map();
    this._metadata = new Map();
  }

  _getId(storeType) {
    return storeType.id;
  }

  unslurp(storeTypes) {
    if (this._getId(storeTypes)) {
      this._unslurpOne(storeTypes);
    } else {
      const iterator = storeTypes.values ? storeTypes.values() : storeTypes;
      for (const storeType of storeTypes) {
        this._unslurpOne(storeType);
      }
    }
  }

  _removeMetadata(metadataId, id) {
    const metadata = this._metadata.get(metadataId);
    if (metadata) return metadata.delete(id);
  }

  _emitChanged(storeType) {
    console.log("Emitting a change...", storeType);
    this.emit("change", storeType);
  }

  smartFetch(fetchId, staleness) {
    const isStale =
      staleness === 0 || new Date().getTime() - staleness >= constants.staleAge;
    if (isStale) return this.fetch(fetchId);
    else return Promise.resolve(undefined);
  }

  _fetchData(fetchId) {
    return Promise.reject(
      new Error("_fetchData must be implemented by inheritors of GenericStore!")
    );
  }

  async fetch(fetchId, fetchType) {
    console.log("Here we are fetching...");
    const storeTypes = await this._fetchData(fetchId, fetchType);
    this.slurp(storeTypes);
    // Not sure this is the best solution
    return storeTypes;
  }

  _unslurpOne(storeType) {
    const id = this._getId(storeType);
    const existing = this.getItem(id);
    if (!existing) return;
    const metadataId = this._getMetadataId(existing);
    if (metadataId) {
      if (this._removeMetadata(metadataId, id)) {
        this._emitChanged(existing);
      }
    }
    this._remove(id);
  }

  slurp(storeTypes, replace) {
    if (this._getId(storeTypes)) {
      this._slurpOne(storeTypes);
    } else {
      const iterator = storeTypes.values ? storeTypes.values() : storeTypes;
      for (const storeType of storeTypes) {
        this._slurpOne(storeType);
      }
    }
  }

  getItem(id) {
    return this._storage.get(id);
  }

  getItemKeys(metadataId) {
    const metadata = this._metadata.get(metadataId);
    if (metadata) {
      return Array.from(metadata.values());
    } else return [];
  }

  getItems(metadataId) {
    const metadata = this._metadata.get(metadataId);
    if (metadata) {
      // Svelte likes arrays
      const items = new Array(metadata.size);
      let i = 0;
      for (const itemId of metadata.values()) {
        items[i++] = this.getItem(itemId);
      }
      return items;
    } else return [];
  }

  _merge(oldItem, newItem) {
    let changed = false;
    for (const newProperty in newItem) {
      oldItem[newProperty] = newItem[newProperty];
      changed = true;
    }
    return changed;
  }

  _store(id, storeType) {
    this._storage.set(id, storeType);
  }

  _remove(id) {
    this._storage.delete(id);
  }

  _storeNew(id, storeType) {
    this._store(id, storeType);
    const metaId = this._getMetadataId(storeType);
    let metadata = this._metadata.get(metaId);
    if (metadata) metadata.add(id);
    else metadata = new Set([id]);
    // Safer to just set() it always otherwise we could have issues
    this._metadata.set(metaId, metadata);
  }

  _getMetadataId(storeType) {
    return storeType.guildId;
  }

  _slurpOne(storeType) {
    const id = this._getId(storeType);
    const existingItem = this.getItem(id);
    if (existingItem) {
      // If we made no changes...
      if (!this._merge(existingItem, storeType)) {
        return;
      }
      // _getId() may give us something different post-merge
      this._store(this._getId(existingItem), existingItem);
    } else {
      this._storeNew(id, storeType);
    }
    this._emitChanged(storeType);
  }
}

export default GenericStore;
