import GenericStore from "./GenericStore";
import SubscriptionStore from "./SubscriptionStore";

export default class RealTimeStore extends GenericStore {
  constructor() {
    super();
    this._keySubscriptions = new Map();
    // (We don't call _setup() because that's the actual store's job)
  }
  // subscriptionKey = "g";

  _subscriptionItemId(subscriptionKey, storeType) {
    if (subscriptionKey == "g") {
      return storeType.guildId;
    } else if (subscriptionKey == "c") {
      return storeType.channelId;
    } else if (subscriptionKey == "u") {
      return storeType.userId;
    } else {
      throw new Error("Unknown subscriptionKey: " + subscriptionKey);
    }
    // return this._metadataId();
  }

  _emitChanged(storeType) {
    console.log("Other emitChanged", this.constructor.name);
    for (const [
      subscriptionKey,
      subscriptions
    ] of this._keySubscriptions.entries()) {
      console.debug(
        this.constructor.name,
        "iterating into keysubscriptions...",
        subscriptionKey,
        subscriptions
      );
      const keyedId = this._subscriptionItemId(subscriptionKey, storeType);
      for (const [component, subscriptionData] of subscriptions.entries()) {
        console.debug(
          this.constructor.name,
          "iterating into keysubscriptionData...",
          subscriptionData,
          keyedId,
          storeType
        );
        if (keyedId == subscriptionData.itemId) {
          console.log(this.constructor.name, "Found matching hook", keyedId);
          subscriptionData.eventHook.apply(component);
        }
      }
    }
    super._emitChanged(storeType);
  }

  async subscribeComponent(itemId, component, eventHook, subscriptionKey) {
    // We just set a default...
    if (!subscriptionKey) subscriptionKey = this.subscriptionKey;
    let subscriptions = this._keySubscriptions.get(subscriptionKey);
    if (!subscriptions) {
      subscriptions = new Map();
      this._keySubscriptions.set(subscriptionKey, subscriptions);
    }
    const existing = subscriptions.get(component);
    if (!existing) {
      if (itemId === null || itemId === undefined) {
        console.debug(
          `Ignoring new subscription on ${subscriptionKey}_${itemId} for component`,
          component,
          "because it's nully"
        );
        return;
      }
      component.on("destroy", () => {
        const currentSubscription = subscriptions.get(component);
        if (!currentSubscription)
          return console.warn(
            "Component destroyed but its subscription doesn't exist?"
          );
        console.debug(
          `(tentative) Unsubscribing from ${
            currentSubscription.subscriptionKey
          }_${currentSubscription.itemId} due to component destruction`
        );
        SubscriptionStore.unsubscribe(
          currentSubscription.subscriptionKey,
          currentSubscription.itemId
        );
        subscriptions.delete(component);
      });
    } else if (
      existing.itemId == itemId &&
      subscriptionKey == existing.subscriptionKey
    ) {
      return console.warn(
        "Requested a subscribe an an already subscribed item on the same component?",
        itemId,
        existing.itemId,
        component,
        existing.subscriptionKey,
        subscriptionKey,
        new Error("Stack")
      );
    } else {
      console.debug(
        `(tentative) Unsubscribing from ${existing.subscriptionKey}_${
          existing.itemId
        } due to updated state of a ${component.constructor.name} (${
          existing.subscriptionKey
        }_${existing.itemId}->${subscriptionKey}_${itemId})`
      );
      SubscriptionStore.unsubscribe(existing.subscriptionKey, existing.itemId);
    }

    console.debug(
      `(tentative) Subscribing to ${subscriptionKey}_${itemId} due to updated state`
    );
    if (itemId === null || itemId === undefined) {
      console.debug(
        `Ignoring subscription to ${subscriptionKey}_${itemId} as it's nully`
      );
    }
    const clean =
      itemId === null || itemId === undefined
        ? true
        : await SubscriptionStore.subscribe(subscriptionKey, itemId, this);
    console.log("Do we need fetching...?", !clean, subscriptionKey, itemId);
    if (
      !clean // ||
      // We may still end up in a scenario where we haven't fetched it at all yet
      // !(this._storage.has(itemId) || this._metadata.has(itemId))
    ) {
      console.log("Fetching...", itemId, this.fetch);
      try {
        await this.fetch(itemId, subscriptionKey);
      } catch (err) {
        console.error("ERROR IN FETCHING", err);
      }
    }

    subscriptions.set(component, {itemId, eventHook, subscriptionKey});
    eventHook.apply(component);
  }
}
