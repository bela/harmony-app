import ChannelStore from "./ChannelStore";
import device from "../js/device";
import GuildStore from "./GuildStore";
import MessageStore from "./MessageStore";
import UserStore from "./UserStore";
import SubscriptionStore from "./SubscriptionStore";
import {Store} from "svelte/store";
import RestClient from "../js/RestClient";
import TokenStore from "../js/TokenStore";

class AppStore extends Store {
  constructor(...opts) {
    super(...opts);
    this.on("state", this.onstate);
    const changed = {};
    for (const key in opts[0]) {
      changed[key] = true;
    }
    this.onstate({current: opts[0], changed, previous: null});
    RestClient.get("/self/styles").then(res => {
      this.set({
        userStyle: res.style
      });
    });
  }

  onGuildsChange() {
    const guilds = GuildStore.getGuilds(true);
    const got = this.get();
    const diff = {
      guilds: Array.from(guilds.values()),
      guildIds: Array.from(guilds.keys()),
      guild: guilds.get(got.guildId)
    };
    if (!diff.guild && got.guildId) {
      diff.channels = null;
      diff.channel = null;
      diff.guildId = null;
    }
    this.set(diff);
  }

  onstate({current, changed}) {
    console.log("[as] ONSTATE!", current, changed);
    if (changed.guildId) {
      ChannelStore.subscribeComponent(
        current.guildId,
        this,
        this.onGuildChannelsChange,
        "g"
      ).then(() => {});

      GuildStore.subscribeComponent(current.guildId, this, () => {}, "g").then(
        () => this.onGuildsChange()
      );
    }
    if (changed.channelId) {
      console.log("[as] channelId changed");
      ChannelStore.subscribeComponent(
        current.channelId,
        this,
        () => {},
        "c"
      ).then(() => {
        console.log("[as] Subbed chan");
        const channel = ChannelStore.getChannel(current.channelId);

        const diff = {channel};

        if (channel.guildId != this.get().guildId) {
          diff.guildId = channel.guildId;
        }
        this.set(diff);
      });
    }
    if (changed.userId) {
      console.log("[as] userId changed");
      UserStore.subscribeComponent(
        current.userId,
        this,
        () => {
          console.log("UserStore changed");
          const user = UserStore.getItem(this.get().userId);
          this.set({
            username: user.username,
            discriminator: user.discriminator,
            avatar: user.avatar
          });
        },
        "u"
      );
    }
  }

  onGuildChannelsChange() {
    const got = this.get();
    const channels = ChannelStore.getItems(got.guildId, true);
    console.log(channels);
    this.set({
      channels,
      channelIds: channels.map(channel => channel.id),
      channel: got.channelId && ChannelStore.getItem(got.channelId)
    });
  }

  async setToken(token) {
    await TokenStore.setToken(token);
    const userId = TokenStore.getUserIdSync(token);
    this.set({token, userId});

    // Don't like this but we have no choice...
    if (token) require("../js/index").default.initialise(token);
  }

  async setChannel(channelId) {
    console.log("setChannel()");
    this.set({channelId});
  }
}

const appStore = new AppStore({
  guildId: null,
  guild: null,
  members: [],
  channels: [],
  channelIds: [],
  channel: null,
  guilds: [],
  modal: null,
  token: TokenStore.getTokenSync(), // TokenStore.getToken(),
  username: "Loading",
  userId: TokenStore.getUserIdSync(),
  discriminator: 0,
  lastWidth: device.lastWidth,
  userStyle: "",
  emojiShape: localStorage.emojiShape || "hmn",
  emojiColor: localStorage.emojiColor || "k2"
});

GuildStore.on("change", function() {
  appStore.onGuildsChange();
});

export default appStore;
