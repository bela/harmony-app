import RealTimeClient from "../js/RealTimeClient";
import constants from "../js/constants";

class SubscriptionStore {
  constructor() {
    this.subscriptionCounts = new Map();
    this.subscriptions = new Map();
    this.subscribedStores = new Map();
  }

  // resourceKey is g, c, u, etc.
  async subscribe(resourceKey, itemId, store) {
    const counts = this.subscriptionCounts.get(resourceKey + itemId);
    let stores = this.subscribedStores.get(resourceKey + itemId);
    if (!stores) {
      stores = new Set();
      this.subscribedStores.set(resourceKey + itemId, stores);
    }

    if (counts) {
      this.subscriptionCounts.set(resourceKey + itemId, counts + 1);
    } else {
      this.subscriptionCounts.set(resourceKey + itemId, 1);
      const hasResumed = await this._subscribe(resourceKey, itemId);
      console.log("Subscribed, here's what we got", hasResumed);
      if (!hasResumed) stores.clear();
    }

    const returns = stores.has(store);

    stores.add(store);

    return returns;
  }

  unsubscribe(resourceKey, itemId) {
    const counts = this.subscriptionCounts.get(resourceKey + itemId);
    if (counts) {
      if (counts <= 1) {
        this.subscriptionCounts.delete(resourceKey + itemId);
        this._unsubscribe(resourceKey, itemId);
      } else {
        this.subscriptionCounts.set(resourceKey + itemId, counts - 1);
      }
    } else {
      console.warn(
        "Unsubscribe requested, but there are no subscriptions?",
        resourceKey,
        itemId,
        new Error("Stack")
      );
    }
  }

  // Actually sends an unsubscribe event
  async _unsubscribe(resourceKey, itemId) {
    await RealTimeClient._unsubscribe({
      [resourceKey]: [itemId]
    });
  }
  async _subscribe(resourceKey, itemId) {
    const subscriptionDetails = await RealTimeClient._subscribe({
      [resourceKey]: [itemId]
    });
    console.log(
      "_subscribe()d",
      subscriptionDetails.d,
      "resourceKey",
      resourceKey,
      "itemId",
      itemId,
      subscriptionDetails.d[resourceKey]
    );
    const resumed = subscriptionDetails.d[resourceKey][itemId];
    return resumed;
  }
}

export default new SubscriptionStore();
