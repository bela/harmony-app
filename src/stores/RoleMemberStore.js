import {EventEmitter} from "events";
import RestClient from "../js/RestClient";
import UserStore from "./UserStore";
import RealTimeClient from "../js/RealTimeClient";
import MessageStore from "./MessageStore";

class RoleMemberStore extends EventEmitter {
  constructor() {
    super();
    this.storage = new Map();
  }

  unslurp(roleMember) {
    const existingRole = this.storage.get(roleMember.roleId);
    if (existingRole) {
      existingRole.delete(roleMember.userId);
      this.emit("change", roleMember);
    }
  }

  slurp(roleMember) {
    console.log("SLURPING A ROLEMEMBER!", roleMember);

    let existingRoleMembers = this.storage.get(roleMember.roleId);
    if (existingRoleMembers) existingRoleMembers.add(roleMember.userId);
    else {
      existingRoleMembers = new Set([roleMember.userId]);
      this.storage.set(roleMember.roleId, existingRoleMembers);
    }

    this.emit("change", roleMember);
  }

  async fetchMemberships(guildId, userId) {
    const res = await RestClient.get(
      `/guilds/${guildId}/members/${userId}/roles`
    );
    for (const roleId of res) {
      this.slurp({
        userId,
        roleId
      });
    }
    return res;
  }

  // getRoleMember(id) {
  //   return this.storage.get(id);
  // }

  getMembers(roleId) {
    return this.storage.get(roleId) || new Set();
  }
}

const store = new RoleMemberStore();

window.roleMemberstore = store;

export default store;
