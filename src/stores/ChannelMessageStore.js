import fastInsert from "../js/fastInsert";
import RealTimeStore from "./RealTimeStore";
import RestClient from "../js/RestClient";
import RealTimeClient from "../js/RealTimeClient";

class ChannelMessageStore extends RealTimeStore {
  constructor() {
    super();
    super._setup();
  }

  storeName = "MESSAGE";
  subscriptionKey = "c";

  _attachListeners() {
    super._attachListeners();
    RealTimeClient.on("ATTACHMENT_CREATE", this.slurpAttachment.bind(this));
  }

  slurpAttachment(attachment) {
    const message = this.getItem(attachment.messageId);
    if (!message)
      return console.warn(
        "Got an ATTACHMENT_CREATE for a message we don't have"
      );
    const found = message.attachments
      .map((a, i) => ({a, i}))
      .find(({a}) => a.id == attachment.id);
    if (found) {
      const attachmentIndex = found.i;
      this._merge(message.attachments[attachmentIndex], attachment);
    } else {
      message.attachments.push(attachment);
    }
    console.log("Attachment created...");
    this._emitChanged(message);
  }

  _createStorages() {
    this._nonces = new Set();
    super._createStorages();
  }

  _getMetadataId(storeType) {
    return storeType.channelId;
  }

  _getId(storeType) {
    console.log("_getId()", storeType, storeType.id, storeType.nonce);
    if (storeType.nonce && this._nonces.has(storeType.nonce)) {
      return storeType.nonce;
    } else {
      return storeType.id;
    }
  }

  _storeNew(id, storeType) {
    console.log("Storing a new...?", storeType, id);
    // Conveniently, we only have to worry about nonce being there on the first event
    if (storeType.nonce && !this._nonces.has(storeType.nonce)) {
      delete storeType.nonce;
      storeType.nonce = undefined;
      delete storeType.nonce;
    }
    this._store(id, storeType);
    const metaId = this._getMetadataId(storeType);
    let metadata = this._metadata.get(metaId);
    if (metadata) fastInsert(metadata, id);
    else metadata = [id];

    this._metadata.set(metaId, metadata);
  }

  _store(id, storeType) {
    if (storeType.nonce) {
      this._storage.set(storeType.nonce, storeType);
    }
    if (storeType.id) {
      this._storage.set(storeType.id, storeType);
    }
  }

  _removeMetadata(metadataId, id) {
    const metadata = this._metadata.get(metadataId);
    if (metadata) {
      const index = metadata.indexOf(id);
      if (index !== -1) {
        metadata.splice(index, 1);
        return true;
      }
    }
  }

  getItems(metadataId) {
    const metadata = this._metadata.get(metadataId);
    if (metadata) {
      return metadata.map(itemId => this.getItem(itemId));
    } else return [];
  }

  async _fetchData(channelId, params = "") {
    return await RestClient.get(
      `/channels/${channelId}/messages${params.startsWith("?") ? params : ""}`
    );
  }
}

window.channelmessagestore = module.exports = new ChannelMessageStore();
